﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObjects : MonoBehaviour {
	public Transform lastPositionPath;
	public Transform lastPositionObstacles;
	public Transform startPositionObstacle;
	public Transform startPositionPlayer;
	public float distanceBetweenTwoPath;
	public List<GameObject> poolObstacles = new List<GameObject>(); 
	public float minDistanceBetweenObstacle = 9;
	public float maxDistanceBetweenObstacle = 14;
	public int numberObstacleBegin = 5;

	// Use this for initialization
	void Start () {
		AddObstacleBegin ();
	}

	void AddObstacleBegin() {
		for (int i = 0; i < numberObstacleBegin; i++) {
			GameObject obs = GetPoolObstacle ();
			if (obs) {
				obs.transform.position = new Vector2 (lastPositionObstacles.position.x + GetDistanceBetweenObstacle (), 
					obs.transform.position.y);
				obs.GetComponent<Obstacle> ().isFrontPlayer = true;
				obs.SetActive (true);
				obs.GetComponent<Obstacle> ().StartUpdate ();
				lastPositionObstacles = obs.transform;
			}
		}
	}

	public GameObject GetPoolObstacle() {
		List<GameObject> listObstacle = new List<GameObject>();
		for (int i = 0; i < poolObstacles.Count; i++) {
			if (!poolObstacles[i].activeInHierarchy) {
				listObstacle.Add(poolObstacles[i]);
			}
		}

		if (listObstacle.Count > 0) {
			return listObstacle [Random.Range (0, listObstacle.Count)];
			listObstacle.Clear ();
		}

		return null;
	}

	public float GetDistanceBetweenObstacle() {
		int distanceDiffrient = Random.Range (0, (int)(maxDistanceBetweenObstacle - minDistanceBetweenObstacle));
		return minDistanceBetweenObstacle + distanceDiffrient;
	}
}
