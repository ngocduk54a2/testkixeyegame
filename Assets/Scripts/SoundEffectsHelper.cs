﻿using UnityEngine;
using System.Collections;

public class SoundEffectsHelper : MonoBehaviour {

	/// <summary>
	/// Singleton
	/// </summary>
//	public static SoundEffectsHelper Instance;
	public static SoundEffectsHelper Instance { get; private set; }

	public AudioClip jumpSound;
	public AudioClip passObstacleSound;
	public AudioClip gameOverSound;

	void Awake()
	{
		// Register the singleton
		if (Instance != null) {
			Debug.Log ("Multiple instances of SoundEffectsHelper!");
		} else {
			Instance = this;
			DontDestroyOnLoad (gameObject);
		}
	}

	void Start()
	{

	}

	public void MakeJumpSound()
	{
		MakeSound(jumpSound);
	}

	public void MakePassObstacleSound()
	{
		MakeSound(passObstacleSound);
	}

	public void MakeOverSound()
	{
		MakeSound(gameOverSound);
	}
		
	private void MakeSound(AudioClip originalClip)
	{
		if (GameObject.FindGameObjectWithTag (Constant.tagPlayer)) {
			AudioSource.PlayClipAtPoint (originalClip, GameObject.FindGameObjectWithTag (Constant.tagPlayer).transform.position);
		} else {
			AudioSource.PlayClipAtPoint (originalClip, transform.position);
		}
	}
}
