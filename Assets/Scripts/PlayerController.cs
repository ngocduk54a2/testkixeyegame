﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
public class PlayerController : MonoBehaviour {
	Rigidbody2D myRigidbody2D;
	public bool gameOver = false;
	public int score = 0;
	public Vector2 velocity = new Vector2 (10, 0);
	public bool jumpAble = true;
	public float forceJump = 750000;
	public float forceJumpHigh = 1000000;
	// Use this for initialization
	void Start () {
		myRigidbody2D = gameObject.GetComponent<Rigidbody2D> ();
		myRigidbody2D.velocity = velocity;
	}
	
//	// Update is called once per frame
	void Update () {
		#if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.A))
			Jump();
		if(Input.GetKeyDown(KeyCode.D))
			JumpHigh();

		if(Input.GetKeyDown(KeyCode.R) && gameOver)
			SceneManager.LoadScene ("GamePlay");
		#endif
	}

	public void Jump()
	{
		if (!gameOver && jumpAble) {
			myRigidbody2D.AddForce (new Vector2 (0, forceJump));
			jumpAble = false;
			SoundEffectsHelper.Instance.MakeJumpSound ();
		}
	}

	public void JumpHigh()
	{
		if (!gameOver && jumpAble) {
			myRigidbody2D.AddForce (new Vector2 (0, forceJumpHigh));
			jumpAble = false;
			SoundEffectsHelper.Instance.MakeJumpSound ();
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		//catch event contact obstacles
		if (coll.gameObject.tag == Constant.tagObstacle) {
			if(!gameOver)
				GameOver ();
		}

		//catch event contact Paths
		if (coll.gameObject.tag == Constant.tagPath) {
			jumpAble = true;
			myRigidbody2D.velocity = velocity;
		}
	}

	void GameOver() {
		gameOver = true;
		int bestScore = PlayerPrefs.GetInt (Constant.keyBestScore);
		if (score > bestScore) {
			//Save best score
			PlayerPrefs.SetInt (Constant.keyBestScore, score);

			//Post score 
//			string localHot = "";
//			string url = localHot + "POST /leaderboard";
//			string JSON_Body = "json\n{\n    \"userName\": \"some-user-name\",\n    \"score\": 120\n}";
//			StartCoroutine(PostRequest(url, JSON_Body));
		}

		SoundEffectsHelper.Instance.MakeOverSound ();
	}

	IEnumerator PostRequest(string url, string bodyJsonString)
	{
		var request = new UnityWebRequest(url, "POST");
		byte[] bodyRaw = new System.Text.UTF8Encoding().GetBytes(bodyJsonString);
		request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
		request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
		request.SetRequestHeader("Content-Type", "application/json");

		yield return request.Send();

		Debug.Log("Response: " + request.downloadHandler.text);
		if (request.isNetworkError)
		{
			Debug.Log("Something went wrong, and returned error: " + request.error);
		}
		else
		{
			if (request.responseCode == 404)
			{
				Debug.Log("Username not found (user has not registered with the leaderboard service)");
			}
			else if (request.responseCode == 405)
			{
				Debug.Log("Invalid Username supplied");
			}
			else if (request.responseCode == 200)
			{
				Debug.Log("Ok !");
			}
			else
			{
				Debug.Log("Request failed (status:" + request.responseCode + ").");
			}
		}
	}
}
