﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuBegin : MonoBehaviour {
	public Button buttonStart;
	public Button buttonQuit;
	public Button buttonClear;
	// Use this for initialization
	void Start () {
		buttonStart.onClick.AddListener (ClickStart);
		buttonQuit.onClick.AddListener (ClickQuit);
		buttonClear.onClick.AddListener (ClickClear);
	}

	void StartPrefabsValue() {
		if (!PlayerPrefs.HasKey (Constant.keyBestScore))
			PlayerPrefs.SetInt (Constant.keyBestScore, 0);
	}
	
	public void ClickStart() {
		SceneManager.LoadScene ("GamePlay");
	}

	public void ClickQuit() {
		Application.Quit ();
	}

	public void ClickClear() {
		PlayerPrefs.DeleteAll();
	}
}
