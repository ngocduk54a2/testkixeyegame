﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;
public class MenuGamePlay : MonoBehaviour {
	public Button buttonPause;
	public Button buttonResume;
	public Button buttonQuit;
	public Button buttonJump;
	public Button buttonJumpHigh;
	public Button buttonReplay;
	public Button buttonHome;
	public Text textScore;
	public Text textBestScore;
	public Text textScoreGameOver;
	public GameObject pauseWindown;
	public GameObject gameoverWindown;
	private PlayerController playerController;
	// Use this for initialization
	void Start () {
		buttonPause.onClick.AddListener (ClickPause);
		buttonResume.onClick.AddListener (ClickResume);
		buttonQuit.onClick.AddListener (ClickQuit);
		buttonJump.onClick.AddListener (ClickJump);
		buttonJumpHigh.onClick.AddListener (ClickJumpHigh);
		buttonReplay.onClick.AddListener (ClickRePlay);
		buttonHome.onClick.AddListener (ClickHome);
		StartCoroutine (MyUpdate());
		if(GameObject.FindGameObjectWithTag(Constant.tagPlayer))
			playerController = GameObject.FindGameObjectWithTag(Constant.tagPlayer).GetComponent<PlayerController>();
		if (!PlayerPrefs.HasKey (Constant.keyShowTutorial))
			PlayerPrefs.SetInt (Constant.keyShowTutorial, 0);
		if (PlayerPrefs.GetInt (Constant.keyShowTutorial) < 3) {
			EditorUtility.DisplayDialog ("Tutorial", "in unity editor press key A to jump, press key D to jump high , \n" +
				"press key R to replay when gameover", "OK");
			PlayerPrefs.SetInt (Constant.keyShowTutorial, PlayerPrefs.GetInt (Constant.keyShowTutorial) + 1);
		}
	}
	
	public void ClickPause() {
		pauseWindown.SetActive (true);
		Time.timeScale = 0;
	}

	public void ClickResume() {
		pauseWindown.SetActive (false);
		Time.timeScale = 1;
	}


	public void ClickQuit() {
		Application.Quit ();
	}

	public void ClickRePlay() {
		SceneManager.LoadScene ("GamePlay");
	}

	public void ClickHome() {
		SceneManager.LoadScene ("Begin");
	}

	public void ClickJump() {
		if (playerController) {
			playerController.Jump ();
		} else {
			playerController = GameObject.FindGameObjectWithTag (Constant.tagPlayer).GetComponent<PlayerController> ();
		}

	}

	public void ClickJumpHigh() {
		if (playerController) {
			playerController.JumpHigh ();
		} else {
			playerController = GameObject.FindGameObjectWithTag (Constant.tagPlayer).GetComponent<PlayerController> ();
		}
	}

	IEnumerator MyUpdate()
	{
		while (true) {
			yield return new WaitForSeconds (Constant.timeMyUpdate);
			textScore.text = playerController.score.ToString ();
			if (playerController.gameOver) {
				gameoverWindown.SetActive (true);
				textScoreGameOver.text = "Your score: " + playerController.score.ToString ();
				textBestScore.text = "Best score: " + PlayerPrefs.GetInt(Constant.keyBestScore).ToString ();
			}
				
		}
	}
}
