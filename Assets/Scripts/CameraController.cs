﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public enum FollowAxes{ X, XY }
	public FollowAxes followAxes;
	public Vector2 offset;

	private Transform thisTransform, playerTransform;
	private PlayerController playerController;

	// Use this for initialization
	void Start () 
	{
		thisTransform = transform;
		playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		playerTransform = playerController.transform;
//		StartCoroutine (FollowPlayer());
	}

	void Update () 
	{
		if (!playerController) {
			playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
			if (playerController)
				playerTransform = playerController.transform;
		}
		if(!playerController.gameOver)
		{
			if(followAxes == FollowAxes.X) 
				thisTransform.position = new Vector3(playerTransform.position.x + offset.x, offset.y, thisTransform.position.z);
			else 
				thisTransform.position = new Vector3(playerTransform.position.x + offset.x, playerTransform.position.y + offset.y, thisTransform.position.z);
		}
	}

//	IEnumerator Wait()
//	{
//		yield return new WaitForSeconds (0.01f);
//		StartCoroutine (FollowPlayer());
//	}
//
//	IEnumerator FollowPlayer()
//	{
//		yield return new WaitForSeconds (0.01f);
//		StartCoroutine (Wait());
//
//		if (!playerController) {
//			playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
//			if (playerController)
//				playerTransform = playerController.transform;
//		}
//		if(!playerController.gameOver)
//		{
//			if(followAxes == FollowAxes.X) 
//				thisTransform.position = new Vector3(playerTransform.position.x + offset.x, offset.y, thisTransform.position.z);
//			else 
//				thisTransform.position = new Vector3(playerTransform.position.x + offset.x, playerTransform.position.y + offset.y, thisTransform.position.z);
//		}
//	}
}
