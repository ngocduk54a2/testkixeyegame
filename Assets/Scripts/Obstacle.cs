﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
	private PlayerController playerController;
	private Transform playerTransform;
	private PoolObjects poolObjects;
	public bool isFrontPlayer = true;
	// Use this for initialization
	void Start () {
		if (GameObject.FindGameObjectWithTag (Constant.tagPlayer)) {
			playerController = GameObject.FindGameObjectWithTag (Constant.tagPlayer).GetComponent<PlayerController> ();
			playerTransform = playerController.transform;
		}

		if (GameObject.FindGameObjectWithTag (Constant.tagPoolObjects)) {
			poolObjects = GameObject.FindGameObjectWithTag (Constant.tagPoolObjects).GetComponent<PoolObjects> ();
		}

		StartUpdate ();
	}

	public void StartUpdate()
	{
		StartCoroutine (MyUpdate());
	}
		
	IEnumerator MyUpdate()
	{
		while (true) {
			yield return new WaitForSeconds (Constant.timeMyUpdate);
			if (!playerController.gameOver) {
				//get score and add new obstacle when pass an obstacle
				if (transform.position.x < playerTransform.position.x && isFrontPlayer) {
					isFrontPlayer = false;
					playerController.score += Constant.scorePassObstacle;

					GameObject obs = poolObjects.GetPoolObstacle ();
					obs.transform.position = new Vector2 (poolObjects.lastPositionObstacles.position.x + poolObjects.GetDistanceBetweenObstacle (), 
						obs.transform.position.y);
					obs.GetComponent<Obstacle> ().isFrontPlayer = true;
					obs.SetActive (true);
					obs.GetComponent<Obstacle> ().StartUpdate ();
					poolObjects.lastPositionObstacles = obs.transform;

					SoundEffectsHelper.Instance.MakePassObstacleSound ();
				}

//			//hide obstacle when go to out of scene
			if (transform.position.x < playerTransform.position.x - Constant.distanceObstacleOutScene) {
				gameObject.SetActive (false);
			}
		}
		}
	}

}
