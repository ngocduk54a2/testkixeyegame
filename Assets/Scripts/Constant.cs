﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant : MonoBehaviour {

	public const int scorePassObstacle = 10;
	public const float distanceObstacleOutScene = 10;
	public const float distancePathOutScene = 10;
	public const float timeMyUpdate = 0.1f;

	public const string keyBestScore = "keyBestScore";
	public const string keyShowTutorial = "keyShowTutorial";


	//tag name
	public const string tagPlayer = "Player";
	public const string tagObstacle = "Obstacle";
	public const string tagPath = "Path";
	public const string tagPoolObjects = "PoolObjects";
}
