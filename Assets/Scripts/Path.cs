﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour {
	public GameObject head;
	public GameObject end;
	private Transform playerTransform;
	private PlayerController playerController;
	// Use this for initialization
	private PoolObjects poolObjects;
	void Start () {
		if (GameObject.FindGameObjectWithTag (Constant.tagPlayer)) {
			playerController = GameObject.FindGameObjectWithTag (Constant.tagPlayer).GetComponent<PlayerController> ();
			playerTransform = playerController.transform;
		}

		if (GameObject.FindGameObjectWithTag (Constant.tagPoolObjects)) {
			poolObjects = GameObject.FindGameObjectWithTag (Constant.tagPoolObjects).GetComponent<PoolObjects> ();

		}

		StartCoroutine (MyUpdate());
	}
	
	IEnumerator MyUpdate()
	{
		while (true) {
			yield return new WaitForSeconds (Constant.timeMyUpdate);
			if (end.transform.position.x < playerTransform.position.x - Constant.distancePathOutScene) {
				if (poolObjects) {
					gameObject.transform.position = new Vector2 (poolObjects.lastPositionPath.position.x + poolObjects.distanceBetweenTwoPath, 
						poolObjects.lastPositionPath.position.y);
					poolObjects.lastPositionPath = transform;
				}
			}

		}
	}
}
